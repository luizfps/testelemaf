package teste;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Parameterized.class)
public class TesteLemaf {

	/*
	 * Observação para rodar a aplicacao deve incluir no classpah o biblioteca
	 * junit5 o jar selenium-server-standalone-3.141.59 Deve-se também baixar o
	 * drive para o navegador google chrome no link
	 * https://chromedriver.storage.googleapis.com/index.html?path=2.45/ Apos baixar
	 * deve setar ele no SetProperty no método initDriver().
	 * 
	 * Este teste é automatizado mais por questão de tempo só pude fazer 2
	 * interações!!!
	 */


	 // webDriver responsavel pela interação com o navegador
	private WebDriver wd = null;

	//campos dos dados que deverao ser preenchidos no cadastro e usados na autenticação
	private String nomes;
	private String cpf;
	private String emails;
	private String senha;
	private String senhaconfirmada;

	//os dados para o teste apenas 2 instancias de teste pois não tive muito tempo
	@Parameters
	public static String[][] data() {
		return new String[][] { { "luiz felipe", "01525403699", "luizfelipe_p@hotmail.com", "32251996", "32251996" },
				{ "", "45370320063", "luizfelipe_p2@hotmail.com", "32251996", "32251996" },

		};

	}

	public TesteLemaf(String nomes, String cpf, String emails, String senha, String senhaconfirmada) {
		super();
		this.nomes = nomes;
		this.emails = emails;
		this.senha = senha;
		this.senhaconfirmada = senhaconfirmada;
		this.cpf = cpf;
	}

	//inicializamos este médodo antes de tudo que seta o driver do chrome
	@Before
	public void initDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/luizfps/Downloads/chromedriver_linux64/chromedriver");
		this.wd = new ChromeDriver();
	}

	@Test
	public void cadastro() throws InterruptedException {

		//abre o navegador com o seguinte endereço
		wd.get("http://gerir.teste.ti.lemaf.ufla.br/#/cadastro");

		wd.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
		wd.manage().timeouts().setScriptTimeout(20, TimeUnit.SECONDS);

		//encontra o elemento específicado na página e seta o seu valor com os parametros passados no teste
		WebElement camponome = wd.findElement(By.name("nome"));
		camponome.sendKeys(nomes);
		// semelhando ao item anterior
		WebElement campocpf = wd.findElement(By.name("cpf"));
		campocpf.sendKeys(cpf);
		// semelhando ao item anterior
		WebElement campoemail = wd.findElement(By.name("email"));
		campoemail.sendKeys(emails);
		// semelhando ao item anterior
		WebElement camposenha = wd.findElement(By.name("senha"));
		camposenha.sendKeys(senha);
		// semelhando ao item anterior
		WebElement campoconfirmasenha = wd.findElement(By.name("confirmar"));
		campoconfirmasenha.sendKeys(senhaconfirmada);
		// semelhando ao item anterior
		WebElement botaocadastrar = wd.findElement(By.className("input-cadastrar"));
		//ativa o botão de cadastrar.
		botaocadastrar.click();

	}

	@Test
	public void autenticacao() {

		wd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		wd.get("http://gerir.teste.ti.lemaf.ufla.br/#/inicio");

		WebElement campocpf = wd.findElement(By.name("cpf"));
		campocpf.sendKeys(cpf);

		WebElement camposenha = wd.findElement(By.name("password"));
		camposenha.sendKeys(senha);

		WebElement botaoentrar = wd.findElement(By.className("input-entrar")); //
		botaoentrar.click();

	}

}
